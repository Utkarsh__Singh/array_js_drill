// Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.

function map(arr, cb) {
    const squares=[]
    for (let index = 0; index < arr.length; index++) {
        squares.push(cb(arr[index],index));
    }
    return squares;
}

// this function is going to call the map function
function main(arr,cb) {
    const mapResult = map(arr, cb);
    return mapResult
}
// const items = [11, 28, 3, 34, 5, 5];
// console.log(main(items));

module.exports = main;