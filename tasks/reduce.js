//! Do NOT use .reduce to complete this function.
    // * How reduce works: A reduce function combines all elements into a single value going from left to right.
    // * Elements will be passed one by one into `cb` along with the `startingValue`.
    // * `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // * `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.


function reduce(arr, cb, startingValue) {
    let indexStart=0
    if (!startingValue) {
        indexStart = 1
        startingValue=arr[0]
    }
    for (let index = indexStart; index < arr.length; index++){
        let reducer = cb(startingValue,arr[index]);
        startingValue=reducer
    }
    return startingValue
}

// this is our callback function
function cb(startingValue, currentValue) {
    return startingValue*currentValue
}

// this function is going to call over reduce()
function main(arr,startingValue=null) {
    const reduceResult = reduce(arr, cb,startingValue);
    // console.log(reduceResult)
    return reduceResult
}

module.exports = main
// const items = [1, 2, 3, 4, 5, 5];
// main(items)