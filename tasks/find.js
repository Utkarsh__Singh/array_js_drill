// ! Do NOT use .includes, to complete this function.
// * Look through each value in `elements` and pass each element to `cb`.
// * If `cb` returns `true` then return that element.
// * Return `undefined` if no elements pass the truth test.


function find(arr, cb) {
    for (let index = 0; index < arr.length; index++){
        if(cb(arr[index])) return arr[index];
    }
}

// this is our callback function which will give us
// value such that (values%5==1)
function cb(lookupValue) {
    // console.log(lookupValue)
    if(lookupValue%5===0) return true;
}

function main(arr, lookupValue) {
    let findResult = find(arr, cb)
    console.log(findResult)
    return findResult
}

module.exports = main

// const items = [1, 2, 3, 4, 5, 5];
// main(items)