// ! Do NOT use .filter, to complete this function.
// * Similar to `find` but you will return an array of all elements that passed the truth test
//* Return an empty array if no elements pass the truth test

function filter(arr, cb) {
    let resultArr=[]
    for (let index = 0; index < arr.length; index++){
        if (cb(arr[index])) {
            resultArr.push(arr[index])
        }
    }
    return resultArr;
}

// driver code
function main(arr,cb) {
    filterResult = filter(arr, cb);
    console.log(filterResult);
    return filterResult
}
module.exports = main