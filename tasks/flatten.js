// * Flattens a nested array (the nesting can be to any depth).
// * Hint: You can solve this using recursion.
// * Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

function flatten(arr) {
    let resultArr=[]
    for (let index = 0; index < arr.length; index++){
        if (arr[index] instanceof Array) {
            let res = flatten(arr[index]);
            resultArr=resultArr.concat(res);
        }
        else(resultArr.push(arr[index]));
    }
    return resultArr
}

module.exports = flatten;