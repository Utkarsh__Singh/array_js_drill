// ! Do NOT use forEach to complete this function.
    // * Iterates over a list of elements, yielding each in turn to the `cb` function.
    // * This only needs to work with arrays.
    // * You should also pass the index into `cb` as the second argument

function each(arr, cb) {
    for (let index = 0; index < arr.length; index++) {
        cb(arr[index],index);
    }
}

// this is our callback function
function cb(number,index) {
     // this function will print elemnts with their index
    console.log(`${number} is at ${index} index`);
}
// this function is going to invoke `each` function.
function main(arr) {
    const eachResult = each(arr, cb);
    return eachResult
}
const items = [1, 2, 3, 4, 5, 5];
// console.log( main(items));
module.exports = main