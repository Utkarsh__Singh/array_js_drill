const map = require('../tasks/map')

// test 1
const items1 = [11, 28, 3, 34, 5, 5];
let expectedOutput1 = items1.map((item) => {
    return item*item
})

// this is our callback function
// this will return number^2
function cb1(element,index) {
    return (element*element)
}

test('should return the squared number array', () => {
    expect(map(items1,cb1)).toEqual(expectedOutput1)
})

// test 2
const items2 = [11, 28, 3, 34, 5, 5];
let expectedOutput2 = items2.map((item) => {
    return item+item
})
// this is our callback function
// this will return number + number
function cb2(element,index) {
    return (element+element)
}

test('should return the squared number array', () => {
    expect(map(items2,cb2)).toEqual(expectedOutput2)
})

