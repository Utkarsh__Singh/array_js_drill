const filter = require('../tasks/filter');
const items = [1, 2, 3, 4, 5, 5];

// test 1

let expectedOutput1 = items.filter((item) =>(item % 2 === 0))
// this is our callback function for filtering out even elements 
// such that (element%2==0)
function cb1(element) {
    if (element % 2 === 0) return true;
}

test('should return all even values', () => {
    expect(filter(items,cb1)).toEqual(expectedOutput1)
})
// test 2

let expectedOutput2 = items.filter((item) =>(item % 2 === 1))
// this is our callback function for filtering out even elements 
// such that (element%2==0)
function cb2(element) {
    if (element % 2 === 1) return true;
}
test('should return all even values', () => {
    expect(filter(items,cb2)).toEqual(expectedOutput2)
})
