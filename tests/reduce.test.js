const reduce = require('../tasks/reduce')
const arr = [1, 2, 3, 4, 5, 5]

let expectedOutput = arr.reduce((item, item2)=>{
    return item*item2
})
// test 1 
test('should return all values multiplied', () => {
    expect(reduce(arr)).toBe(expectedOutput)
})

let startingValue = 2
let expectedOutput2 = arr.reduce((item, item2)=>{
    return item*item2
},startingValue)

// test 2
test('should return all values multiplied', () => {
    expect(reduce(arr,startingValue)).toBe(expectedOutput2)
})