const each = require('../tasks/each')
const items = [1, 2, 3, 4, 5, 5];

// sice we know forEach return nothing/ undefined
test('forEach should return undefined', () => {
    expect(each(items)).toBeUndefined();
});