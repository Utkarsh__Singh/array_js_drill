const find=require('../tasks/find')
const arr = [1, 2, 3, 4, 5, 5];

let expectedOutput1 = arr.find((item) => item % 5 === 0)
// console.log(expectedOutput)

// this is our callback function which will give us
// value such that (values%5==1)
function cb1(lookupValue) {
    // console.log(lookupValue)
    if(lookupValue%5===0) return true;
}
// test 1
test('should return the frist value such that (value%5==0', () => {
    expect(find(arr,cb1)).toBe(expectedOutput1)
})



let expectedOutput2 = arr.find((item) => item*3 === 6)

// this is our callback function which will give us
// value such that (values*3==6)
function cb2(lookupValue) {
    // console.log(lookupValue)
    if(lookupValue*3===6) return true;
}
// test 2

test('should return the frist value such that (value*3===6', () => {
    expect(find(arr,cb2)).toBe(expectedOutput2)
})