const flatten = require('../tasks/flatten')
// use this to test 'flatten'
const nestedArray = [1, [2], [[3]], [[[4]]]];


let expectedOutput1 = nestedArray.flat(Infinity)
// console.log(expectedOutput1)

test('should return flattened array such that array=> [1, 2, 3, 4];', () => {
    expect(flatten(nestedArray)).toEqual(expectedOutput1)
})
